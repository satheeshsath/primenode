'use strict';
const examresultService = require('../services/examresult.service')

class ExamResultController {

  constructor() {}

  async createExamResult (req, res) {
     let data = req.body;
     // data.date = new Date();
     try {
       console.log('create exam result called..');
       let result = await examresultService.createExamresult(data);
       res.json({ status: 201, id: result._id });
     } catch (e) {
       console.log('e : ' + JSON.stringify(e));
       res.status(201).json({ status: 500, message: "Internal Server Error" });
     }
   }

  async updateExamInfo (req, res) {
    let data = req.body;
    // console.log('data : ' + JSON.stringify(data));
    let conditions = { _id: data.examId }
    , update = { $set: { status: data.status, questionInfo: data.questionInfo, mark: data.mark, summary: data.summary, markInPercentage: data.markInPercentage, totalTimeTaken: data.totalTimeTaken }}
    , options = { multi: false, strict: false };
    try {
      let numAffected = await examresultService.updateExamresults(conditions, update, options);
      res.status(201).json({ status: 201, message: "Succesfully Updation Teacher" });
    } catch (e) {
      console.log('Error : ' + e);
      res.status(201).json({ status: 500, message: "Internal Server Error!" });
    }
  }

  async getReportsByEmail (req, res) {
     try {
       console.log('getReportsByEmail called..');
       let email = req.params.email;
       // let query = { email: email, $or: [{ status: 'Completed' }, { status: 'Incompleted' }] };
       let query = { email: email, status: { "$ne": 'Not Started' }};
       let reports = await examresultService.getExamresults(query, { _id: 1, date: 1, questionGroupName: 1, questionSubgroupName: 1, markInPercentage: 1 }, 0, 0);
       res.json({ status: 201, reports: reports });
     } catch (e) {
       console.log('e : ' + JSON.stringify(e));
       res.status(201).json({ status: 500, message: "Internal Server Error" });
     }
   }
   async getReportById (req, res) {
      try {
        console.log('getReportById called..');
        let id = req.params.id;
        let query = { _id: id }
        let report = await examresultService.getExamresults(query, null, 0, 0);
        res.json({ status: 201, report: report[0] });
      } catch (e) {
        console.log('e : ' + JSON.stringify(e));
        res.status(201).json({ status: 500, message: "Internal Server Error" });
      }
    }

   async getAllReports (req, res) {
      try {
        console.log('get all reports called..');
        let query = { status: { "$ne": 'Not Started' }};
        let reports = await examresultService.getExamresults(query, { _id: 1, name: 1, email: 1, date: 1, questionGroupName: 1, questionSubgroupName: 1, markInPercentage: 1 }, 0, 0);
        res.json({ status: 201, reports: reports });
      } catch (e) {
        console.log('e : ' + JSON.stringify(e));
        res.status(201).json({ status: 500, message: "Internal Server Error" });
      }
    }

}
module.exports = new ExamResultController();
