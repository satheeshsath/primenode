'use strict';
const userService = require('../services/user.service')
const emailer = require('../util/emailer')
const otpService = require('../util/otp.service')

class UserController {

  constructor() { }

  async verifyUser (req, res) {
    console.log('create user called..');
     let data = req.body;
     // console.log('data : ' + JSON.stringify(data));
     try {
       let query = { email: data.email };
       let result = await userService.getUsers(query);
       console.log('result : ' + JSON.stringify(result));
       if(result.length){ // if already member  -- email found
         if(result[0].password){ // already member with password
           console.log('Password found. Duplicate Entry...');
           res.json({ status: 409, message: "Duplicate Entry" });
         } else { // member via OAuth -- verify OTP and update password and name in DB
           console.log('password not found. OAuth member...');
           const otp = await otpService.generateOTP();
           const mailTo = data.email;
           const subject = 'prime psc';
           const message = otp;
           const emailResult = await emailer.sendMail(mailTo, subject, message);
           res.json({ status: 203, message: 'Third party copy', otp: otp });
         }
       } else { // new member, // send and verify OTP and create user
         console.log('new member....');
         const otp = await otpService.generateOTP();
         const mailTo = data.email;
         const subject = 'prime psc';
         const message = otp;
         console.log('otp : ' + otp);
         const emailResult = await emailer.sendMail(mailTo, subject, message);
         res.json({ status: 200, message: 'New Member', otp: otp });
       }
     } catch (e) {
       console.log('e : ' + JSON.stringify(e));
       res.status(201).json({ status: 500, message: "Internal Server Error" });
     }
   }

  async createUserByPassword (req, res) {
    console.log('create user called..');
     let data = req.body;
     console.log('data : ' + JSON.stringify(data));
     data.date = new Date();
     data.role = 'Student';
     try {
       console.log('create user called..');
       let result = await userService.createUser(data);
       res.json({ status: 201 });
     } catch (e) {
       console.log('e : ' + JSON.stringify(e));
       res.status(201).json({ status: 500, message: "Internal Server Error" });
     }
   }

  async updateUserPassword (req, res) {
    let data = req.body;
    let conditions = { email: data.email }
    , update = { $set: { password: data.password, name: data.name }}
    , options = { multi: false, strict: false };
    try {
      let numAffected = await userService.updateUsers(conditions, update, options);
      res.status(201).json({ status: 201, message: "Succesfully Updation" });
    } catch (e) {
      console.log('Error : ' + e);
      res.status(201).json({ status: 500, message: "Internal Server Error!" });
    }
  }

  async oAuthSignIn (req, res) {
     let data = req.body;
     // console.log('data : ' + JSON.stringify(data));
     console.log('oAuthSignIn called..');
     try {
       let query = { email: data.email };
       let result = await userService.getUsers(query);
       if(result.length){ // if already member  -- email found
         console.log('already member..');
         let isProviderFound = false;
         //console.log('result[0] : ' + JSON.stringify(result[0]));
           if(result[0] && (result[0].oAuth) && (result[0].oAuth.length)){
            for(let i=0; i < result[0].oAuth.length; i++){
              if(result[0].oAuth[i].provider == data.provider){ isProviderFound= true; }
            }
          }
          if(isProviderFound) { // if OAuth i/f found
            console.log('if OAuth i/f found..');
            res.json({ status: 201, role: result[0].role || 'Student', message: 'User Already Registered' });
          } else {
            console.log('if OAuth i/f not found..');
            //console.log('result : ' + JSON.stringify(result));
            let oAuthInfo = result[0].oAuth || [];
            oAuthInfo.push({ name: data.name, provider: data.provider });
            console.log('if OAuth i/f not found..');
            let conditions = { email: data.email }
            , update = { $set: { oAuth: oAuthInfo }}
            , options = { multi: false, strict: false };
            let numAffected = await userService.updateUsers(conditions, update, options);
            console.log('if OAuth i/f not found..');
            res.status(201).json({ status: 201, role: result[0].role, message: "User Updated" });
          }
       } else { // new member
         console.log('new member..');
         let user = { email: data.email, date: new Date(), name: data.name, role: 'Student', oAuth: [{ name: data.name, provider: data.provider }] }
         let result = await userService.createUser(user);
         res.json({ status: 201, message: 'User created' });
       }
     } catch (e) {
       console.log('Error called..');
       console.log('e : ' + JSON.stringify(e));
       res.status(201).json({ status: 500, message: "Internal Server Error" });
     }
   }

  async loginUserByPassword (req, res) {
    let data = req.body;
    console.log('loginUserByPassword called..');
    try {
      let query = { email: data.email };
      let result = await userService.getUsers(query);
      if(result.length){ // if already member  -- email found
        if(result[0].password == data.password){
          res.status(201).json({ status: 201, name: result[0].name, email: result[0].email, role: result[0].role  });
        } else {
          res.json({ status: 401, message: 'Password Incorrect' });
        }
      } else { // new member
        res.json({ status: 401, message: 'User Credentials Incorrect' });
      }
    } catch (e) {
      console.log('e : ' + JSON.stringify(e));
      res.status(201).json({ status: 500, message: "Internal Server Error" });
    }
  }

}
module.exports = new UserController();
