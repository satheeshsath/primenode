'use strict';
const questionService = require('../services/question.service')
const groupService = require('../services/group.service')
const subgroupService = require('../services/subgroup.service')

class QuestionController {

  constructor() {}

  async getFreeQuestions (req, res) {
     let subgroupId = req.params.id;
     let responseData = {};
     try {
       let subgroups = await subgroupService.getSubgroups({ _id: subgroupId }, null, 0, 0); // query, projection fields, skip, limit
       let groups = await groupService.getGroups({ _id: subgroups[0].questionGroupId }, null, 0, 0);
       let query = { questionSubgroupId: subgroupId, isFreeQuestion: true };
       let questions = await questionService.getRandomQuestions(query, subgroups[0].questionCount || 10);
       responseData.subgroup = subgroups[0];
       responseData.group = groups[0];
       responseData.questions = questions;
       res.json({ status: 201, responseData: responseData });
     } catch (e) {
       console.log('e : ' + JSON.stringify(e));
       res.status(201).json({ status: 500, message: "Internal Server Error." });
     }
   }
   async getPremiumQuestions (req, res) {
      let subgroupId = req.params.id;
      let responseData = {};
      try {
        let subgroups = await subgroupService.getSubgroups({ _id: subgroupId }, null, 0, 0); // query, projection fields, skip, limit
        let groups = await groupService.getGroups({ _id: subgroups[0].questionGroupId }, null, 0, 0);
        let query = { questionSubgroupId: subgroupId, isPremiumQuestion: true };
        let questions = await questionService.getRandomQuestions(query, subgroups[0].questionCount || 10);
        responseData.subgroup = subgroups[0];
        responseData.group = groups[0];
        responseData.questions = questions;
        res.json({ status: 201, responseData: responseData });
      } catch (e) {
        console.log('e : ' + JSON.stringify(e));
        res.status(201).json({ status: 500, message: "Internal Server Error." });
      }
    }

    async getAllQuestions(req, res) {
      let responseData = {};
      try {
        let groups = await groupService.getGroups({ }, null, 0, 0); // query, projection fields, skip, limit
        let subgroups = await subgroupService.getSubgroups({ }, { _id: 1, questionGroupId: 1, questionSubgroupName: 1 }, 0, 0); // query, projection fields, skip, limit
        let questions = await questionService.getQuestions({}, { _id: 1, questionName: 1, questionGroupId: 1, questionSubgroupId: 1 }, 0, 0);
        let quesArray = [];
        for(let q=0; q < questions.length; q++){
          let ques = {};
          ques._id = questions[q]._id;
          ques.questionName = questions[q].questionName;
          for(let g=0; g < groups.length; g++){
            if(questions[q].questionGroupId == groups[g]._id){
              // questions[q].questionGroupName = groups[g].questionGroupName;
              ques.questionGroupName = groups[g].questionGroupName;
            }
          }
          for(let s=0; s < subgroups.length; s++){
            if(questions[q].questionSubgroupId == subgroups[s]._id){
              // questions[q].questionSubgroupName = subgroups[s].questionSubgroupName;
              ques.questionSubgroupName = subgroups[s].questionSubgroupName;
            }
          }
          // if(ques.questionGroupName && ques.questionSubgroupName){
             quesArray.push(ques);
          // }
        }
        responseData.groups = groups;
        responseData.subgroups = subgroups;
        responseData.questions = quesArray;
        res.json({ status: 201, responseData: responseData });
      } catch (e) {
        console.log('e : ' + JSON.stringify(e));
        res.status(201).json({ status: 500, message: "Internal Server Error." });
      }
    }
    async getQuestionBygroupId(req, res) {
      let responseData = {};
      try {
        let questionGroupId = req.params.id;
        let query = {}
        let groups = await groupService.getGroups({ _id: questionGroupId }, null, 0, 0); // query, projection fields, skip, limit
        let subgroups = await subgroupService.getSubgroups({ }, { _id: 1, questionGroupId: 1, questionSubgroupName: 1 }, 0, 0); // query, projection fields, skip, limit
        let questions = await questionService.getQuestions({ questionGroupId: questionGroupId }, { _id: 1, questionName: 1, questionGroupId: 1, questionSubgroupId: 1 }, 0, 0);
        let quesArray = [];
        for(let q=0; q < questions.length; q++){
          let ques = {};
          ques._id = questions[q]._id;
          ques.questionName = questions[q].questionName;
          for(let g=0; g < groups.length; g++){
            if(questions[q].questionGroupId == groups[g]._id){
              // questions[q].questionGroupName = groups[g].questionGroupName;
              ques.questionGroupName = groups[g].questionGroupName;
            }
          }
          for(let s=0; s < subgroups.length; s++){
            if(questions[q].questionSubgroupId == subgroups[s]._id){
              // questions[q].questionSubgroupName = subgroups[s].questionSubgroupName;
              ques.questionSubgroupName = subgroups[s].questionSubgroupName;
            }
          }
          quesArray.push(ques);
        }
        responseData.groups = groups;
        responseData.subgroups = subgroups;
        responseData.questions = quesArray;
        res.json({ status: 201, responseData: responseData });
      } catch (e) {
        console.log('e : ' + JSON.stringify(e));
        res.status(201).json({ status: 500, message: "Internal Server Error." });
      }
    }
}
module.exports = new QuestionController();
