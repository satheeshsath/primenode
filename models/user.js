const mongoose = require('mongoose');
const Schema = mongoose.Schema;

// Define collection and schema
let User = new Schema({
    name: { type: String },
    userID: { type: String },
    email: { type: String },
    password: { type: String },
    role: { type: String },
    oAuth: { type: Object },
    date : { type: Date }
}, {
    collection: 'user_master'
})

module.exports = mongoose.model('User', User)
