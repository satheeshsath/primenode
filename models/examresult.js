const mongoose = require('mongoose');
const Schema = mongoose.Schema;

// Define collection and schema
let Exam = new Schema({
    name: { type: String },
    id: { type: String },
    email: { type: String },
    questionGroupId: { type: String },
    questionGroupName: { type: String },
    questionSubgroupId: { type: String },
    questionSubgroupName: { type: String },
    summary: { type: String },
    mark: { type: String },
    markInPercentage: { type: String },
    totalTimeTaken: { type: Number },
    status: { type: String },
    userID : { type: String },
    isFreeTest : { type: Boolean },
    isPremiumTest : { type: Boolean },
    questionInfo: { type: Object },
    date : { type: Date },
}, {
    collection: 'exam_master'
})

module.exports = mongoose.model('Exam', Exam)
