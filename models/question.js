const mongoose = require('mongoose');
const Schema = mongoose.Schema;

// Define collection and schema
let Question = new Schema({
  questionName: { type: String },
  questionGroupId: { type: String },
  questionSubgroupId: { type: String },
  option1: { type: String },
  option2: { type: String },
  option3: { type: String },
  option4: { type: String },
  isFreeQuestion: { type: Boolean },
  isPremiumQuestion: { type: Boolean },
  answer: { type: String }
}, {
  collection: 'question_master'
})

module.exports = mongoose.model('Question', Question)
