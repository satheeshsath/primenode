'use strict';
const GroupModel = require('../models/group');

class GroupService {
  constructor() {}

  async createGroup (group) {
    console.log('group from : ' + JSON.stringify(group));
    let newGroup = new GroupModel(group);
    try {
      let savedGroup = await newGroup.save()
      return savedGroup;
    } catch (e) {
      // throw Error("Error while Creating Group")
      console.log('e : ' + JSON.stringify(e));
        throw e
    }
  }

  async getGroups (query, projectionFields, skip, limit) {
    try {
      let groups = await GroupModel.find(query, projectionFields) // find all Groups
      .skip(skip) // skip the first limit items
      .limit(limit);
      return groups;
    } catch (e) {
      throw e
    }
  }

  async updateGroups(conditions, update, options) {
    try {
      let numAffected = await GroupModel.update(conditions, update, options);
      return numAffected;
    } catch (e) {
      throw e;
    }
  }

}
module.exports = new GroupService();
