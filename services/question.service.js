'use strict';
const QuestionModel = require('../models/question');

class QuestionService {
  constructor() {}

  async createQuestion (question) {
    console.log('question from : ' + JSON.stringify(question));
    let newQuestion = new QuestionModel(question);
    try {
      let savedQuestion = await newQuestion.save()
      return savedQuestion;
    } catch (e) {
      // throw Error("Error while Creating Question")
      console.log('e : ' + JSON.stringify(e));
        throw e
    }
  }

  async getRandomQuestions(query, limit) {
    try {
      let questions = await QuestionModel.aggregate([
        { $match: query },
        { $sample: { size: limit } }
      ]);
      return questions;
    } catch (e) {
      throw e
    }
  }

  async getQuestions (query, projectionFields, skip, limit) {
    try {
      let questions = await QuestionModel.find(query, projectionFields) // find all Questions
      .skip(skip) // skip the first limit items
      .limit(limit);
      return questions;
    } catch (e) {
      throw e
    }
  }

  async updateQuestions(conditions, update, options) {
    try {
      let numAffected = await QuestionModel.update(conditions, update, options);
      return numAffected;
    } catch (e) {
      throw e;
    }
  }

}
module.exports = new QuestionService();
