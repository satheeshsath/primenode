'use strict';
const ExamresultModel = require('../models/examresult');

class ExamresultService {
  constructor() {}

  async createExamresult (examresult) {
    console.log('examresult from : ' + JSON.stringify(examresult));
    let newExamresult = new ExamresultModel(examresult);
    try {
      let savedExamresult = await newExamresult.save()
      return savedExamresult;
    } catch (e) {
      // throw Error("Error while Creating Examresult")
      console.log('e : ' + JSON.stringify(e));
        throw e
    }
  }

  async getExamresults (query, projectionFields, skip, limit) {
    try {
      let examresults = await ExamresultModel.find(query, projectionFields) // find all examresults
      .skip(skip) // skip the first limit items
      .limit(limit);
      return examresults;
    } catch (e) {
      throw e
    }
  }

  async updateExamresults (conditions, update, options) {
    try {
      let numAffected = await ExamresultModel.update(conditions, update, options);
      return numAffected;
    } catch (e) {
      throw e;
    }
  }

}
module.exports = new ExamresultService();
