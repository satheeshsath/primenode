'use strict';
const SubgroupModel = require('../models/subgroup');

class SubgroupService {
  constructor() {}

  async createSubgroup (subgroup) {
    console.log('subgroup from : ' + JSON.stringify(subgroup));
    let newSubgroup = new SubgroupModel(subgroup);
    try {
      let savedSubgroup = await newSubgroup.save()
      return savedSubgroup;
    } catch (e) {
      // throw Error("Error while Creating Subgroup")
      console.log('e : ' + JSON.stringify(e));
        throw e
    }
  }

  async getSubgroups (query, projectionFields, skip, limit) {
    try {
      let subgroups = await SubgroupModel.find(query, projectionFields) // find all Subgroups
      .skip(skip) // skip the first limit items
      .limit(limit);
      return subgroups;
    } catch (e) {
      throw e
    }
  }

  async updateSubgroups(conditions, update, options) {
    try {
      let numAffected = await SubgroupModel.update(conditions, update, options);
      return numAffected;
    } catch (e) {
      throw e;
    }
  }

}
module.exports = new SubgroupService();
