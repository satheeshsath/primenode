'use strict';
const UserModel = require('../models/user');

class UserService {
  constructor() {}

  async createUser (user) {
    console.log('user from : ' + JSON.stringify(user));
    let newUser = new UserModel(user);
    try {
      let savedUser = await newUser.save()
      return savedUser;
    } catch (e) {
      // throw Error("Error while Creating User")
      console.log('e : ' + JSON.stringify(e));
        throw e
    }
  }

  async getUsers (query, projectionFields, skip, limit) {
    try {
      let users = await UserModel.find(query, projectionFields) // find all Users
      .skip(skip) // skip the first limit items
      .limit(limit);
      return users;
    } catch (e) {
      throw e
    }
  }

  async updateUsers(conditions, update, options) {
    try {
      let numAffected = await UserModel.update(conditions, update, options);
      return numAffected;
    } catch (e) {
      throw e;
    }
  }

}
module.exports = new UserService();
