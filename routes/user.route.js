const express = require('express');
const app = express();
const userRoute = express.Router();

let User = require('../models/user');
// Add User
userRoute.route('/userSave').post((req, res, next) => {
  User.create(req.body, (error, data) => {
    if (error) {
      return next(error)
    } else {
      res.status(200).json({ message: 'User added successfully.!' });
    }
  })
});

const userController = require('../controllers/user.controller');

userRoute.post('/verifyUser', userController.verifyUser);
userRoute.post('/createUserByPassword', userController.createUserByPassword);
userRoute.post('/updateUserPassword', userController.updateUserPassword);
userRoute.post('/oAuthSignIn', userController.oAuthSignIn);
userRoute.post('/loginUserByPassword', userController.loginUserByPassword);

module.exports = userRoute;
