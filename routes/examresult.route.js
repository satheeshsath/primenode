const express = require('express');
const app = express();
const examRoute = express.Router();

let Exam = require('../models/examresult');

// Add User
examRoute.route('/saveResult').post((req, res, next) => {
  Exam.create(req.body, (error, data) => {
    if (error) {
      return next(error)
    } else {
      res.status(200).json({ message: 'Exam Submitted.!' });
    }
  })
});

const examresult = require('../controllers/examresult.controller');

examRoute.post('/createExamResult', examresult.createExamResult);
examRoute.post('/updateExamInfo', examresult.updateExamInfo);
examRoute.get('/getReportsByEmail/:email', examresult.getReportsByEmail);
examRoute.get('/getReportById/:id', examresult.getReportById);
examRoute.get('/getAllReports', examresult.getAllReports);

module.exports = examRoute;
